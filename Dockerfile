FROM node:current-alpine3.16

RUN addgroup -S appgroup && adduser -S appuser -G appgroup

ENV APP_HOME=/build
RUN mkdir $APP_HOME && chown -R appuser:appgroup $APP_HOME
RUN npm i -g serve

USER appuser
COPY --chown=appuser:appgroup build $APP_HOME

EXPOSE 3000
CMD ["serve","-s","-n","build"]